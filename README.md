# MkDocs on S3 project sample

This project sample shows the usage of the _to be continuous_ templates:

* MkDocs
* S3

The project builds and deploys a static MkDocs site to [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)
on `master` branches, and on S3 on any other branch.

## MkDocs template features

This project makes a very basic use of the MkDocs template, with only default configuration.

It also uses the **pages** variant, that implements a deployment job to [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).

## S3 template features

This project uses the following features from the GitLab CI S3 template:

* Defines mandatory `$S3_ENDPOINT_HOST`,
* Defines mandatory `$S3_ACCESS_KEY` and `$S3_SECRET_KEY` as the project variables (secrets),
* Overrides the default `$S3_DEPLOY_FILES` to upload the MkDocs output build dir,
* Disables any environment but review environments.

The GitLab CI S3 template also implements [environments integration](https://gitlab.com/to-be-continuous/samples/maven-on-openshift/environments)
in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).
