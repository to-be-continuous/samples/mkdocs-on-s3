
# Foo

XML code here:

```xml
<foo>
  <item id=12>
    <question>The Answer to the Ultimate Question of Life, the Universe, and Everything</question>
    <answer>42</answer>
  </item>
</foo>
```
